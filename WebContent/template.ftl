<#ftl encoding="utf-8">
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title></title>
    <style>
        table {width: 95%;}
        tr:nth-child(1) td {font-weight: bold;color: red;}
        tr:nth-child(2) td:nth-child(1) a {color: green;}
        tr:nth-child(2) td:nth-child(2) a {color: orange;}
        tr:nth-child(2) td:nth-child(3) a {color: black;}
        tr:nth-child(2) td:nth-child(4) a {color: red;}
        a:hover {text-decoration: none;}
    </style>
</head>
<body>
<table>
    <tr>
        <td colspan="5">
            <#if alertsArray[0]?has_content>
                Платежи <#list alertsArray[0]?values as accDocId>№${accDocId[0]}<#sep>, </#list> задержаны по требованию ИБ.<br>
            </#if>
            <#if alertsArray[1]?has_content>
                Переводы C2С с карт <#list alertsArray[1]?values as cardNumber>${cardNumber[0]}<#sep>, </#list> отменены по требованию ИБ.<br>
            </#if>
            <#if alertsArray[2]?has_content>
                Платежи СБП на <#list alertsArray[2]?values as contactNumber>${contactNumber[0]}<#sep>, </#list> отменены по требованию ИБ.<br>
            </#if>
            <#if show_action_buttons> Просьба сверить платежи с клиентом и нажать на ссылки по результатам:</#if>
        </td>
    </tr>
    <tr>
        <#if show_action_buttons>
            <#list ['Платежи подтверждены', 'Не удалось дозвониться', 'Неверный тел. номер', 'Клиент не подтвердил платежи'] as title>
                <td><a onclick="return confirm('Подтверждаете нажатие кнопки ${title}?')"
                       href="${url}?cifid=${cifid?c}&amp;empid=${empid}&amp;actid=${title?index}&sessionid=${sessionid}">${title}</a>
                </td>
            </#list>
        </#if>
        <td>По всем вопросам обращаться на группу RBRU-IS_Investigation</td>
    </tr>
</table>
</body>
</html>
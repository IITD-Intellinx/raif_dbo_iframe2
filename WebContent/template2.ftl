<#ftl encoding="utf-8">
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title></title>
    <#if url?? && cifid?? && empid??>
        <meta http-equiv="refresh" content="5; url=${url}?cifid=${cifid?c}&amp;empid=${empid}&amp;"/>
    </#if>
    <style>
        span {
            color: red;
            font-weight: bold;
        }
    </style>
</head>
<body>
<#if sr == 1>
    <span>Запрос составлен неверно.</span>
<#elseif sr == 2>
    <span>Не удалось установить соединение.</span>
<#elseif sr == 3>
    <span>Внутренняя ошибка сервиса.</span>
<#elseif sr == 6>
    <span>Сессия просрочена или отсутсвует.</span>
<#else>
    Action ${actid} accepted
</#if>
</body>
</html>
# README 
### Configuration
```properties
#Web Servlet
http.url=http://localhost:8080/raif_dbo_iframe2_war

#Intellinx
http.backend.url.fetch.alerts=http://[C2C_HOST_NAME]:8080/raif/api/REST/siebel-alerts/alert-ids?cifid=
http.backend.url=http://[FRONT_HOST_NAME]:8080/raif/api/REST/siebel-naked/execute
http.backend.url.c2c=http://[FRONT_HOST_NAME]:8080/raif/api/REST/siebel-c2c/execute
http.backend.url.sbp=http://[FRONT_HOST_NAME]:8080/raif/api/REST/siebel-sbp/execute
http.backend.login=wsUser
http.backend.password=wsUser
```

### Deployment instructions ####
.war файл:

`${catalina.base}/webapps/raif_dbo_iframe2.war`

Файл настроек:

`${catalina.base}/conf/app2.properties`

Файл настроек логирования (опционально):

`${catalina.base}/conf/iframe2_logback.xml`

Без файла настроек логирования, логи будут писать в `${catalina.base}/logs/iitd_iframe2_default.log`

На linux дистрибутивах проставить необходимые права командами `chmod` и `chown`

package ru.iitdgroup.solution.web.managers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

public class HttpSessionInstancesAccessManager {
    private static final Logger logger = LoggerFactory.getLogger(HttpSessionInstancesAccessManager.class);

    private static volatile HttpSessionInstancesAccessManager instance;

    private final Map<String, HttpSession> sessions;

    private HttpSessionInstancesAccessManager() {
        sessions = new HashMap<>();
    }

    public static HttpSessionInstancesAccessManager getInstance() {
        HttpSessionInstancesAccessManager localInstance = instance;

        if (localInstance == null) {
            synchronized (HttpSessionInstancesAccessManager.class) {
                localInstance = instance;
                if (localInstance == null) {
                    try {
                        instance = new HttpSessionInstancesAccessManager();
                        localInstance = instance;
                    } catch (Exception e) {
                        logger.error("exception on init {}", HttpSessionInstancesAccessManager.class.getName(), e);
                    }
                }
            }
        }

        return localInstance;
    }

    public static HttpSession getSession(String sessionId) {
        return getInstance().sessions.get(sessionId);
    }

    public static void removeSession(String sessionId) {
        getInstance().sessions.remove(sessionId);
    }

    public static void setSession(String sessionId, HttpSession session) {
        getInstance().sessions.put(sessionId, session);
    }
}
package ru.iitdgroup.solution.web.managers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iitdgroup.solution.web.constants.PropertiesParams;
import ru.iitdgroup.solution.web.decryption.PasswordEncryption;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

public class PropertiesManager {
    private static final Logger logger = LoggerFactory.getLogger(PropertiesManager.class);

    private static final String CONFIG_NAME = "app2.properties";

    private static volatile PropertiesManager instance;

    private Properties properties;

    private final PasswordEncryption passwordEncryption;

    public PropertiesManager() {
        try {
            File configDir = new File(System.getProperty("catalina.base"), "conf");

            properties = new Properties();

            logger.info("Try to read properties from app2.properties in webapp classpath");
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(CONFIG_NAME);
            if (inputStream != null) {
                logger.info("...load propeties from webapp classpath");
                properties.load(inputStream);
            } else {
                logger.warn("Can't find app2.properties file on classpath");
            }
            logger.info("Try to read properties from tomcat conf folder");

            File configFile = new File(configDir, CONFIG_NAME);
            if (configFile.exists()) {
                logger.info("...load propeties from tomcat conf folder");
                InputStream stream = new FileInputStream(configFile);
                properties.load(stream);
            } else {
                logger.warn("Can't find app2.properties file on tomcat conf folder");
            }
        } catch (Exception e) {
            logger.error("Can't init properties", e);
        }
        logger.info( PropertiesParams.MASTER_KEY_FILE_LOCATION_ENV + "=" + System.getenv(PropertiesParams.MASTER_KEY_FILE_LOCATION_ENV) + "=");
        this.passwordEncryption = System.getenv(PropertiesParams.MASTER_KEY_FILE_LOCATION_ENV) == null ? null
                : getPasswordEncryptionInstance(System.getenv(PropertiesParams.MASTER_KEY_FILE_LOCATION_ENV));
    }

    public static PropertiesManager getInstance() {
        PropertiesManager localInstance = instance;

        if (localInstance == null) {
            synchronized (PropertiesManager.class) {
                localInstance = instance;
                if (localInstance == null) {
                    try {
                        instance = localInstance = new PropertiesManager();
                    } catch (Exception e) {
                        logger.error("exception on init PropertiesManager", e);
                    }
                }
            }
        }

        return localInstance;
    }

    public String getProperty(String name) {
        return getProperty(name, null);
    }

    public String getProperty(String name, String defaultValue) {
        String property = properties.getProperty(name);
        if (property == null) {
            return defaultValue;
        }
        if (property.startsWith("enc:")) {
            return decrypt(property.substring(4));
        }
        return property;
    }

    public int getIntProperty(String name) {
        String property = getProperty(name);

        return Integer.parseInt(property);
    }

    /**
     * Allows to get a property as an array. Each value of the array has to be separated from the others by a comma.
     *
     * @param name - a property name.
     * @return - an array of trimmed strings or, if the property is not set, an empty array.
     */
    public String[] getArrayProperty(String name) {
        String raw = getProperty(name);

        return raw != null && !raw.isEmpty() ? raw.trim().split("\\s*,\\s*") : new String[0];
    }

    public void printAllProperties() {
        logger.info("--Started with properties: ");

        Set<Entry<Object, Object>> entrySet = properties.entrySet();
        for (Entry<Object, Object> entry : entrySet) {
            logger.info("{}={}", entry.getKey(), maskPassword(entry));
        }

        logger.info("--END");
    }

    private Object maskPassword(Entry<Object, Object> entry) {
        return entry.getKey().toString().toLowerCase().contains("password") ? "***" : entry.getValue();
    }

    private String decrypt(String property) {
        return this.passwordEncryption == null ? property : this.passwordEncryption.decrypt(property);
    }

    private PasswordEncryption getPasswordEncryptionInstance(String masterKeyPath) {
        logger.debug("Loading masterfile.");
        return PasswordEncryption.loadKeyFromFile(new File(masterKeyPath));
    }
}

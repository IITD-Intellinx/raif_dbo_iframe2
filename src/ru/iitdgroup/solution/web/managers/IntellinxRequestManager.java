package ru.iitdgroup.solution.web.managers;

import static ru.iitdgroup.solution.web.constants.PropertiesParams.HTTP_BACKEND_URL;
import static ru.iitdgroup.solution.web.constants.PropertiesParams.HTTP_BACKEND_URL_C2C;
import static ru.iitdgroup.solution.web.constants.PropertiesParams.HTTP_BACKEND_URL_SBP;

import ru.iitdgroup.solution.web.client.BasicAuthClient;
import ru.iitdgroup.solution.web.constants.ESimpleResponse;
import ru.iitdgroup.solution.web.constants.PropertiesParams;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IntellinxRequestManager extends BasicAuthClient {
    private static final Logger logger = LoggerFactory.getLogger(IntellinxRequestManager.class);

    private static final PropertiesManager propManager = PropertiesManager.getInstance();
    private static final ObjectMapper mapper = new ObjectMapper();

    private static final String SESSION_ID = "sessionid";
    private static final String CIFID = "cifid";
    private static final String EMPID = "empid";
    private static final String ACTID = "actid";
    private static final String URL = "url";
    private static final String ALERTS_IDS = "alerts";
    private static final String ALERTS_IDS_ARRAY = "alertsArray";
    private static final String SHOW_ACTION_BUTTONS = "show_action_buttons";

    private static final int MIN_ACTION_ID = 0;
    private static final int MAX_ACTION_ID = 3;
    private static final int MAX_EMPID_LENGTH = 10;

    private static final int RUR_ALERTS_POSITION = 0;
    private static final int C2C_ALERTS_POSITION = 1;
    private static final int SBP_ALERTS_POSITION = 2;

    private static final String ARRAY_PARAM_NAME_MASK = "%s%%5B%s%%5D";

    private static final String EMPID_PATTERN = "^[A-Za-z0-9]+$";

    public static ESimpleResponse processRequest(HttpServletRequest request) throws IOException {
        String cifIdString = request.getParameter(CIFID);
        String empId = request.getParameter(EMPID);
        String actionIdString = request.getParameter(ACTID);

        validateParameters(cifIdString, empId);
        long cifId = parseCifId(cifIdString);

        request.setAttribute(CIFID, cifId);
        request.setAttribute(EMPID, empId);
        request.setAttribute(URL, propManager.getProperty(PropertiesParams.HTTP_URL));

        if (actionIdString == null) {
            return fetchAlertIds(request, empId, cifId);
        }

        return triggerAutomaticAction(request, empId, actionIdString, cifId);
    }

    private static void validateParameters(String cifIdString, String empIdString) {
        if (null == cifIdString || null == empIdString) {
            throw new IllegalArgumentException("cifid and empid should not be null");
        }

        if (empIdString.isEmpty()
                || empIdString.length() > MAX_EMPID_LENGTH
                || !Pattern.matches(EMPID_PATTERN, empIdString)) {
            throw new NumberFormatException(EMPID + " should not be longer than " + MAX_EMPID_LENGTH
                    + " symbols and contain only digits and latin letters");
        }
    }

    private static long parseCifId(String cifIdString) {
        long cifId = Long.parseLong(cifIdString);

        if (cifId < 1) {
            throw new NumberFormatException("CIF ID should be positive non-zero number");
        }

        return cifId;
    }

    private static ESimpleResponse fetchAlertIds(HttpServletRequest request,
                                                 String empId,
                                                 long cifId) throws IOException {
        logger.debug("Received request for {} by {}", cifId, empId);
        String idsMapString = sendGetRequest(
                propManager.getProperty(PropertiesParams.HTTP_BACKEND_URL_FETCH_ALERTS) + cifId,
                propManager.getProperty(PropertiesParams.HTTP_BACKEND_LOGIN),
                propManager.getProperty(PropertiesParams.HTTP_BACKEND_PASSWORD)
        );

        Map<Long, String[]>[] alertsArray =
                mapper.readValue(idsMapString, new TypeReference<Map<Long, String[]>[]>() {});
        if (hasAlerts(alertsArray)) {
            request.getSession(true).setAttribute(ALERTS_IDS_ARRAY, alertsArray);
            request.setAttribute(SESSION_ID, request.getSession().getId());
            request.setAttribute(ALERTS_IDS_ARRAY, alertsArray);
            request.setAttribute(SHOW_ACTION_BUTTONS, !hasAlertsInFilteredStatuses(alertsArray));

            logger.debug("Response for {} contains menu", cifId);
            return ESimpleResponse.MENU;
        } else {
            logger.debug("Response for {} is empty", cifId);
            return ESimpleResponse.EMPTY;
        }
    }

    private static boolean hasAlerts(Map<Long, String[]>[] alertsArray) {
        if (alertsArray == null || alertsArray.length != 3) {
            logger.warn("Alerts array should not be empty and contain only 2 elements");
            return false;
        }

        Map<Long, String[]> rurAlerts = alertsArray[RUR_ALERTS_POSITION];
        Map<Long, String[]> c2cAlerts = alertsArray[C2C_ALERTS_POSITION];
        Map<Long, String[]> sbpAlerts = alertsArray[SBP_ALERTS_POSITION];
        if (rurAlerts == null || c2cAlerts == null || sbpAlerts == null) {
            logger.warn("Alerts collections should not be null (rur alerts = {}, c2c alerts = {}, sbp alerts = {})",
                    rurAlerts, c2cAlerts, sbpAlerts);
            return false;
        }

        return !(rurAlerts.isEmpty() && c2cAlerts.isEmpty() && sbpAlerts.isEmpty());
    }

    private static boolean hasAlertsInFilteredStatuses(Map<Long, String[]>[] alertsArray) {
        List<String> filters = Arrays.asList(propManager.getArrayProperty(PropertiesParams.ALERT_STATUS_FILTER));

        List<String> statuses = Stream.of(alertsArray)
                .flatMap(map -> map.entrySet().stream())
                .map(entry -> entry.getValue()[1])
                .collect(Collectors.toList());

        return statuses.removeAll(filters);
    }

    private static ESimpleResponse triggerAutomaticAction(HttpServletRequest request,
                                                          String empId,
                                                          String actionIdString,
                                                          long cifId) throws IOException {
        int actionId = parseActionId(empId, actionIdString, cifId);

        String sessionId = request.getParameter(SESSION_ID);

        if (null == sessionId || sessionId.isEmpty()) {
            logger.warn("sessionId cannot be null or empty when action executing");
            return ESimpleResponse.INVALID_SESSION;
        }

        request.setAttribute(ACTID, actionId);
        Map<Long, String[]>[] alertsArray;
        try {
            alertsArray = (Map<Long, String[]>[]) HttpSessionInstancesAccessManager.getSession(sessionId)
                    .getAttribute(ALERTS_IDS_ARRAY);
        } catch (NullPointerException e) {
            logger.warn("Session is already expired");

            if (null != request.getSession(false)) {
                request.getSession(false).invalidate();
            }

            return ESimpleResponse.INVALID_SESSION;
        }

        ESimpleResponse result = sendActionTriggerRequests(empId, cifId, actionId, alertsArray);
        HttpSessionInstancesAccessManager.getSession(sessionId).invalidate();

        return result;
    }

    private static int parseActionId(String empId, String actionIdString, long cifId) {
        int actionId = Integer.parseInt(actionIdString);

        logger.debug("Received action {} request for {} by {}", actionId, cifId, empId);

        if (actionId > MAX_ACTION_ID || actionId < MIN_ACTION_ID) {
            throw new NumberFormatException(
                    ACTID + " should be in the range from " + MIN_ACTION_ID + " to " + MAX_ACTION_ID
            );
        }

        return actionId;
    }

    private static ESimpleResponse sendActionTriggerRequests(String empId,
                                                             long cifId,
                                                             int actionId,
                                                             Map<Long, String[]>[] alertsArray) throws IOException {
        ESimpleResponse indiResponse =
                sendSingleRequest(empId, cifId, actionId, alertsArray[RUR_ALERTS_POSITION], HTTP_BACKEND_URL);

        ESimpleResponse c2cResponse =
                sendSingleRequest(empId, cifId, actionId, alertsArray[C2C_ALERTS_POSITION], HTTP_BACKEND_URL_C2C);

        ESimpleResponse sbpResponse =
                sendSingleRequest(empId, cifId, actionId, alertsArray[SBP_ALERTS_POSITION], HTTP_BACKEND_URL_SBP);

        return indiResponse == c2cResponse && indiResponse == sbpResponse
                ? indiResponse
                : ESimpleResponse.FAILED;
    }

    private static ESimpleResponse sendSingleRequest(String empId,
                                                     long cifId,
                                                     int actionId,
                                                     Map<Long, String[]> alertsMap,
                                                     String httpBackendUrl) throws IOException {
        if (alertsMap.size() > 0) {
            return sendPostRequest(
                    propManager.getProperty(httpBackendUrl),
                    propManager.getProperty(PropertiesParams.HTTP_BACKEND_LOGIN),
                    propManager.getProperty(PropertiesParams.HTTP_BACKEND_PASSWORD),
                    String.format(MASK_PARAMS, CIFID, cifId)
                            + String.format(MASK_PARAMS, EMPID, empId)
                            + convertToPostArray(alertsMap.keySet())
                            + String.format(MASK_PARAMS_LAST, ACTID, actionId)
            );
        }

        return ESimpleResponse.SUCCESS;
    }

    private static String convertToPostArray(Set<Long> list) {
        StringBuilder arrayParam = new StringBuilder();

        if (list.isEmpty()) {
            return arrayParam.toString();
        }

        int i = 0;
        for (Long alertId : list) {
            arrayParam.append(
                    String.format(MASK_PARAMS, String.format(ARRAY_PARAM_NAME_MASK, ALERTS_IDS, i++), alertId)
            );
        }

        return arrayParam.toString();
    }
}

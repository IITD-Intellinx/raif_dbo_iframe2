package ru.iitdgroup.solution.web.client;

import ru.iitdgroup.solution.web.constants.ESimpleResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class BasicAuthClient {
    private static final Logger logger = LoggerFactory.getLogger(BasicAuthClient.class);

    private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    private static final String AUTH_MASK = "%s:%s";
    private static final String AUTH_HEADER_MASK = "Basic %s";

    protected static final String MASK_PARAMS = "%s=%s&";
    protected static final String MASK_PARAMS_LAST = "%s=%s";

    protected static ESimpleResponse sendPostRequest(final String urlValue,
                                                     final String login,
                                                     final String password,
                                                     final String data) throws IOException {
        try {
            URL url = new URL(urlValue);
            byte[] postData = data.getBytes(StandardCharsets.UTF_8);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", CONTENT_TYPE);
            connection.setRequestProperty("Content-Length", Integer.toString(postData.length));
            connection.setRequestProperty(
                    "Authorization",
                    String.format(
                            AUTH_HEADER_MASK,
                            new String(Base64.encodeBase64(
                                    String.format(AUTH_MASK, login, password).getBytes(StandardCharsets.UTF_8)
                            ))
                    )
            );

            try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                wr.write(postData);
            } catch (ConnectException connectException) {
                logger.error("Cannot write post data", connectException);
                return ESimpleResponse.ERR_CONNECTION;
            }

            int statusCode = connection.getResponseCode();
            logger.debug("got response status {}", statusCode);

            if (statusCode == HttpServletResponse.SC_OK) {
                try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                    String inputLine;
                    StringBuilder response = new StringBuilder();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    logger.debug("got response {}", response);
                    return ESimpleResponse.SUCCESS;
                }
            } else {
                logger.error("got bad status {}", statusCode);
                return ESimpleResponse.FAILED;
            }
        } catch (MalformedURLException e) {
            logger.error("invalid url {}. check configuration file", urlValue);
            return ESimpleResponse.FAILED;
        }
    }

    protected static String sendGetRequest(String urlValue, String login, String password) throws IOException {
        try {
            URL url = new URL(urlValue);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", CONTENT_TYPE);
            connection.setRequestProperty(
                    "Authorization",
                    String.format(
                            AUTH_HEADER_MASK,
                            new String(Base64.encodeBase64(
                                    String.format(AUTH_MASK, login, password).getBytes(StandardCharsets.UTF_8)
                            ))
                    )
            );

            int statusCode = connection.getResponseCode();
            logger.debug("Got response status {}", statusCode);

            if (statusCode == HttpServletResponse.SC_OK) {
                try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                    String inputLine;
                    StringBuilder response = new StringBuilder();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    logger.debug("Got response {}", response);
                    return response.toString();
                }
            } else {
                logger.error("Got bad status {}", statusCode);
            }
        } catch (MalformedURLException e) {
            logger.error("Invalid url {}. Check configuration file", urlValue);
        }

        return null;
    }
}

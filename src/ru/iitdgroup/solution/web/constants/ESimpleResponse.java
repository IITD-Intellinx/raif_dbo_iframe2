package ru.iitdgroup.solution.web.constants;

public enum ESimpleResponse {
    MENU(0),
    FAILED(1),
    ERR_CONNECTION(2),
    ERR_INTERNAL_ERROR(3),
    EMPTY(4),
    SUCCESS(5),
    INVALID_SESSION(6);

    private final int id;

    ESimpleResponse(int iId) {
        this.id = iId;
    }

    public int getValue() {
        return id;
    }
}

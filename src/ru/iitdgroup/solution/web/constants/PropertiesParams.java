package ru.iitdgroup.solution.web.constants;

public class PropertiesParams {
    private PropertiesParams() {
    }

    public static final String HTTP_URL = "http.url";
    public static final String HTTP_BACKEND_URL_FETCH_ALERTS = "http.backend.url.fetch.alerts";
    public static final String HTTP_BACKEND_URL = "http.backend.url";
    public static final String HTTP_BACKEND_URL_C2C = "http.backend.url.c2c";
    public static final String HTTP_BACKEND_URL_SBP = "http.backend.url.sbp";
    public static final String HTTP_BACKEND_LOGIN = "http.backend.login";
    public static final String HTTP_BACKEND_PASSWORD = "http.backend.password";
    public static final String ALERT_STATUS_FILTER = "alert.status.filter";
    public static final String MASTER_KEY_FILE_LOCATION_ENV = "MASTERKEY_PATH";
}

package ru.iitdgroup.solution.web.decryption;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.AlgorithmParameters;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Base64.Decoder;

public class PasswordEncryption {
    private static final String NEW_ALGORITHM = "PBEWithHmacSHA256AndAES_128";
    private static final String NEW_CYPHER_INSTANCE = NEW_ALGORITHM + "/CBC/PKCS5Padding";
    private static final String LEGACY_ALGORITHM = "PBEWithSHA1AndDESede";
    private static final String LEGACY_CIPHER_INSTANCE = LEGACY_ALGORITHM + "/CBC/PKCS5Padding";
    private static final String VERSION_STRING = "VERSION:";
    private static final String ENCRYPTION_VERSION = VERSION_STRING + "002";

    private static final String ERROR_CANNOT_FIND_MASTER_KEY_FILE = "Cannot find master key file: ";
    private static final String ERROR_INVALID_PASSWORD_LENGTH_IN_HEADER = "Password header is incorrect, check length: ";
    private static final String ERROR_INVALID_PASSWORD_ENCODING = "Invalid password encoding";
    private static final String ERROR_PASSWORD_LONGER_THAN_EXPECTED = "Password string is longer than expected: ";

    private static final Decoder base64Decoder = Base64.getDecoder();

    private final SecretKey key;
    private final boolean useNewAlgorithm;

    private PasswordEncryption(SecretKey key, boolean useNewAlgorithm) {
        this.key = key;
        this.useNewAlgorithm = useNewAlgorithm;
    }

    public static PasswordEncryption loadKeyFromFile(File keyFile) {
        if (!keyFile.isFile()) {
            throw new DecryptionException(ERROR_CANNOT_FIND_MASTER_KEY_FILE + keyFile.getAbsolutePath());
        } else {
            try (FileInputStream fileInputStream = new FileInputStream(keyFile)) {
                try (BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream))) {
                    final String base64Password = br.readLine();
                    byte[] bytePassword = base64Decoder.decode(base64Password);
                    final byte[] byteVersionString = VERSION_STRING.getBytes(StandardCharsets.UTF_8);
                    int version = 0;
                    if (bytePassword.length > VERSION_STRING.length() && bytePassword[0] == byteVersionString[0]
                            && bytePassword[1] == byteVersionString[1] && bytePassword[2] == byteVersionString[2]
                            && bytePassword[3] == byteVersionString[3] && bytePassword[4] == byteVersionString[4]
                            && bytePassword[5] == byteVersionString[5] && bytePassword[6] == byteVersionString[6]
                            && bytePassword[7] == byteVersionString[7]) {
                        int versionLength = ENCRYPTION_VERSION.getBytes(StandardCharsets.UTF_8).length;
                        String versionString = new String(bytePassword, 0, versionLength, StandardCharsets.UTF_8);
                        version = Integer.parseInt(versionString.split(":")[1]);
                        final byte[] tmp = new byte[bytePassword.length - versionLength];
                        System.arraycopy(bytePassword, versionLength, tmp, 0, tmp.length);
                        bytePassword = tmp;
                    }
                    final SecretKey key = version > 0
                            ? createSecretKey(bytePassword)
                            : createOldSecretKey(bytePassword);
                    return new PasswordEncryption(key, version > 0);
                }
            } catch (Exception ex) {
                throw new DecryptionException(ex.getMessage(), ex);
            }
        }
    }

    private static SecretKey createSecretKey(byte[] bytePassword) {
        return new SecretKeySpec(bytePassword, NEW_ALGORITHM);
    }

    private static SecretKey createOldSecretKey(byte[] bytePassword)
            throws InvalidKeySpecException, NoSuchAlgorithmException {
        final String password = new String(bytePassword);
        final PBEKeySpec spec = new PBEKeySpec(password.toCharArray());
        return SecretKeyFactory.getInstance(LEGACY_ALGORITHM).generateSecret(spec);
    }

    public String decrypt(String encryptedPassword) {
        try (ByteArrayInputStream byteArrayInputStream =
                     new ByteArrayInputStream(base64Decoder.decode(encryptedPassword))) {
            try (DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream)) {
                final Cipher cipher = Cipher.getInstance(this.useNewAlgorithm
                        ? NEW_CYPHER_INSTANCE
                        : LEGACY_CIPHER_INSTANCE);
                final AlgorithmParameters parameters = AlgorithmParameters.getInstance(this.useNewAlgorithm
                        ? NEW_ALGORITHM
                        : LEGACY_ALGORITHM);

                final int encodedParameterLength = dataInputStream.readInt();
                final byte[] encodedParameter = new byte[encodedParameterLength];
                dataInputStream.readFully(encodedParameter);
                parameters.init(encodedParameter);
                cipher.init(2, this.key, parameters);
                final int passwordLength = dataInputStream.readInt();
                if (passwordLength < 0 || passwordLength > 1024) {
                    throw new DecryptionException(ERROR_INVALID_PASSWORD_LENGTH_IN_HEADER + passwordLength);
                }

                final byte[] encodedPassword = new byte[passwordLength];
                dataInputStream.readFully(encodedPassword);
                if (dataInputStream.available() > 0) {
                    throw new DecryptionException(ERROR_PASSWORD_LONGER_THAN_EXPECTED + passwordLength);
                }

                return new String(cipher.doFinal(encodedPassword));
            }
        } catch (IOException ex) {
            throw new DecryptionException(ERROR_INVALID_PASSWORD_ENCODING, ex);
        } catch (Exception ex) {
            throw new DecryptionException(ex.getMessage(), ex);
        }
    }
}

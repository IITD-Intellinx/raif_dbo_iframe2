package ru.iitdgroup.solution.web.listeners;

import ru.iitdgroup.solution.web.managers.HttpSessionInstancesAccessManager;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class AntiBrowserProtectionHttpSessionListener implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent event) {
        HttpSessionInstancesAccessManager.setSession(event.getSession().getId(), event.getSession());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        HttpSessionInstancesAccessManager.removeSession(event.getSession().getId());
    }
}

package ru.iitdgroup.solution.web.listeners;

import ru.iitdgroup.solution.web.managers.HttpSessionInstancesAccessManager;
import ru.iitdgroup.solution.web.managers.PropertiesManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class WebAppContextListener implements ServletContextListener {
    private static Logger logger = LoggerFactory.getLogger(WebAppContextListener.class);

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {
        // nothing to destroy
    }

    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
        logger.debug("Start init web app");

        PropertiesManager pm = PropertiesManager.getInstance();
        pm.printAllProperties();

        HttpSessionInstancesAccessManager.getInstance();

        logger.debug("End init web app");
    }
}
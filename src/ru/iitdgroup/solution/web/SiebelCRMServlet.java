package ru.iitdgroup.solution.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.iitdgroup.solution.web.constants.ESimpleResponse;
import ru.iitdgroup.solution.web.constants.PropertiesParams;
import ru.iitdgroup.solution.web.managers.IntellinxRequestManager;
import ru.iitdgroup.solution.web.managers.PropertiesManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/", loadOnStartup = 1)
public class SiebelCRMServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(SiebelCRMServlet.class);
    private static final String PARAM_SR = "sr";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");

        try {
            ESimpleResponse simpleResponse = IntellinxRequestManager.processRequest(request);
            request.setAttribute(PARAM_SR, simpleResponse.getValue());
            switch (simpleResponse) {
                case MENU:
                    request.getServletContext().getRequestDispatcher("/template.ftl").forward(request, response);
                    return;
                case EMPTY:
                    response.setStatus(HttpServletResponse.SC_OK);
                    return;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            logger.warn("invalid request parameters", e);
            request.setAttribute(PARAM_SR, ESimpleResponse.FAILED.getValue());
        } catch (Exception e) {
            logger.warn("exception on process request", e);
            request.setAttribute(PARAM_SR, ESimpleResponse.ERR_INTERNAL_ERROR.getValue());
        }

        request.setAttribute("url", PropertiesManager.getInstance().getProperty(PropertiesParams.HTTP_URL));
        request.getServletContext().getRequestDispatcher("/template2.ftl").forward(request, response);
    }
}